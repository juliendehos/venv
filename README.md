# venv

## description

a simple sh script for using python virtualenv 

```
$ venv
usage: venv <command> ...
  - list:       venv l
  - install:    venv i <venv name> [python path]
  - activate:   venv a <venv name>
  - deactivate: venv d
  - remove:     venv r <venv name>
```

## installation

```
curl https://gitlab.com/juliendehos/venv/raw/master/venv.sh -o ~/opt/venv.sh --create-dir
# edit config params in ~/opt/venv.sh
echo '. ${HOME}/opt/venv.sh' >> ~/.bashrc
source ~/.bashrc
```

## quick-start

For example, we want a virtual environment for using the sphinx documentation generator:

- create a venv (`mydoc`) and install python packages:

    ```
    venv i mydoc python3
    venv a mydoc
    pip install sphinx alabaster sphinx_rtd_theme docutils 
    venv d
    ```

- activate the `mydoc` venv and start working:

    ```
    venv a mydoc
    sphinx-quickstart
    ...
    venv d
    ```

