# this file should be sourced, you cannot run it directly

################################ config params ################################

# path storing the directories of the venvs
VENV_VENVS_PATH="${HOME}/opt/venvs"

# virtualenv binary 
VENV_BIN="virtualenv"
#VENV_BIN="virtualenv3"

# python interpreter 
#VENV_PYTHON="python"
VENV_PYTHON="python3"

###############################################################################

mkdir -p ${VENV_VENVS_PATH}

venv_activated=""

venv_usage() {
    echo "usage: ${VENV_NAME} <command> ..."
    echo "  - list:       ${VENV_NAME} l"
    echo "  - install:    ${VENV_NAME} i <venv name> [python path]"
    echo "  - activate:   ${VENV_NAME} a <venv name>"
    echo "  - deactivate: ${VENV_NAME} d"
    echo "  - remove:     ${VENV_NAME} r <venv name>"
}

venv_activate() {
    # check venv directory
    VENV_FULLPATH=${VENV_VENVS_PATH}/$1 
    if ! [ -d "${VENV_FULLPATH}" ] ; then
        echo "error: no venv \"$1\""
        return 1
    fi
    # check venv activate script
    VENV_ACTIVATE_PATH=${VENV_VENVS_PATH}/$1/bin/activate
    if ! [ -f "${VENV_ACTIVATE_PATH}" ] ; then
        echo "error: venv \"$1\" is invalid (no ${VENV_ACTIVATE_PATH})"
        return 1
    fi
    # activate venv 
    . ${VENV_ACTIVATE_PATH}
    venv_activated="$1"
    echo "info: venv \"$1\" activated (${VENV_FULLPATH})"
}

venv_deactivate() {
    if [ "${venv_activated}" != "" ] ; then 
        deactivate
        VENV_TMP=${venv_activated}
        venv_activated=""
        echo "info: venv \"${VENV_TMP}\" deactivated"
    fi 
}

venv_remove() {
    # check venv
    VENV_FULLPATH=${VENV_VENVS_PATH}/$1 
    if ! [ -d "${VENV_FULLPATH}" ] ; then
        echo "error: no venv \"$1\""
        return 1
    fi
    # deactivate venv
    if [ "${venv_activated}" == "$1" ] ; then 
        venv_deactivate
    fi
    # remove venv
    VENV_FULLPATH=${VENV_VENVS_PATH}/$1 
    rm -rf ${VENV_FULLPATH} 
    echo "info: venv \"$1\" removed (${VENV_FULLPATH})"
}

venv_list() {
    ls ${VENV_VENVS_PATH}
}

venv_install() {
    # check venv name
    if [ "$1" == "" ] ; then
        venv_usage 
        echo "error: invalid venv name"
        return 1
    fi
    if [ "$2" != "" ] ; then
        VENV_PYTHON=$2
    fi
    # check if venv already exists
    VENV_FULLPATH=${VENV_VENVS_PATH}/$1 
    if [ -d "${VENV_FULLPATH}" ] ; then
        echo "error: venv \"$1\" already exist"
        return 1
    fi
    # check if the virtualenv binary exists
    if ! which "${VENV_BIN}" &> /dev/null ; then
        echo "error: invalid virtualenv binary (${VENV_BIN})"
        echo "       edit ~/opt/venv.sh and source ~/.bashrc"
        return 1
    fi
     # check if the python interperter exists
    if ! which "${VENV_PYTHON}" &> /dev/null ; then
        echo "error: unknown python version (${VENV_PYTHON} does not exist)"
        echo "       install ${VENV_PYTHON}, or edit ~/opt/venv.sh and source ~/.bashrc"
        return 1
    fi
    # create venv
    VENV_FULLPATH=${VENV_VENVS_PATH}/$1 
    ${VENV_BIN} -p ${VENV_PYTHON} --no-site-packages ${VENV_FULLPATH}
    echo "info: venv \"$1\" created at ${VENV_FULLPATH} using ${VENV_PYTHON}"
}

venv() {
    if [ $# -lt 1 ] ; then
        venv_usage 
        return
    fi

    case $1 in
        "l") venv_list ;;
        "i") venv_install $2 $3 ;;
        "a") venv_activate $2 ;;
        "d") venv_deactivate ;;
        "r") venv_remove $2 ;;
        *) venv_usage; echo "error: invalid command" ;;
    esac
}

